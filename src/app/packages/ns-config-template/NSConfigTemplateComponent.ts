/*
 Copyright 2020 TATA ELXSI

 Licensed under the Apache License, Version 2.0 (the 'License');
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: SANDHYA JS (sandhya.j@tataelxsi.co.in)
*/
/**
 * @file NS Config Template details Component.
 */
import { HttpHeaders } from '@angular/common/http';
import { Component, Injector, OnInit } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { ERRORDATA, MODALCLOSERESPONSEDATA } from 'CommonModel';
import { ComposePackages } from 'ComposePackages';
import { DataService } from 'DataService';
import { environment } from 'environment';
import { LocalDataSource } from 'ng2-smart-table';
import { NSConfigTemplateActionComponent } from 'NSCONFIGTEMPLATEACTION';
import { NSCONFIG, NSConfigData } from 'NSCONFIGTEMPLATEMODEL';
import { RestService } from 'RestService';
import { Subscription } from 'rxjs';
import { SharedService } from 'SharedService';
import { VNFData } from 'VNFDModel';

/**
 * Creating component
 * @Component takes NSConfigTemplateComponent.html as template url
 */
@Component({
    selector: 'app-ns-config-template',
    templateUrl: './NSConfigTemplateComponent.html',
    styleUrls: ['./NSConfigTemplateComponent.scss']
})
/** Exporting a class @exports NSConfigTemplateComponent */
export class NSConfigTemplateComponent implements OnInit {
    /** To inject services @public */
    public injector: Injector;

    /** Data of smarttable populate through LocalDataSource @public */
    public dataSource: LocalDataSource = new LocalDataSource();

    /** handle translate @public */
    public translateService: TranslateService;

    /** Columns list of the smart table @public */
    public columnLists: object = {};

    /** Settings for smarttable to populate the table with columns @public */
    public settings: object = {};

    /** Check the loading results @public */
    public isLoadingResults: boolean = true;

    /** Give the message for the loading @public */
    public message: string = 'PLEASEWAIT';

    /** Class for empty and present data @public */
    public checkDataClass: string;

    /** Instance of the rest service @private */
    private restService: RestService;

    /** dataService to pass the data from one component to another @private */
    private dataService: DataService;

    /** Formation of appropriate Data for LocalDatasource @private */
    private nsConfigData: NSConfigData[] = [];

    /** Contains all methods related to shared @private */
    private sharedService: SharedService;

    /** Controls the header form @private */
    private headers: HttpHeaders;

    /** Instance of the modal service @private */
    private modalService: NgbModal;

    /** Instance of subscriptions @private */
    private generateDataSub: Subscription;

    constructor(injector: Injector) {
        this.injector = injector;
        this.restService = this.injector.get(RestService);
        this.dataService = this.injector.get(DataService);
        this.sharedService = this.injector.get(SharedService);
        this.translateService = this.injector.get(TranslateService);
        this.modalService = this.injector.get(NgbModal);
    }

    /**
     * Lifecyle Hooks the trigger before component is instantiate
     */
    public ngOnInit(): void {
        this.generateColumns();
        this.generateSettings();
        this.generateData();
        this.headers = new HttpHeaders({
            'Content-Type': 'application/gzip',
            Accept: 'application/json',
            'Cache-Control': 'no-cache, no-store, must-revalidate, max-age=0'
        });
        this.generateDataSub = this.sharedService.dataEvent.subscribe((): void => { this.generateData(); });
    }

    /** smart table Header Colums @public */
    public generateColumns(): void {
        this.columnLists = {
            name: { title: this.translateService.instant('NAME'), width: '25%' },
            identifier: { title: this.translateService.instant('IDENTIFIER'), width: '25%' },
            nsdId: { title: this.translateService.instant('PAGE.INSTANCEINSTANTIATE.NSID'), width: '25%' },
            created: { title: this.translateService.instant('Created'), width: '15%' },
            Actions: {
                name: 'Action', width: '15%', filter: false, sort: false, type: 'custom',
                title: this.translateService.instant('ACTIONS'),
                valuePrepareFunction: (cell: VNFData, row: VNFData): VNFData => row, renderComponent: NSConfigTemplateActionComponent
            }
        };
    }

    /** smart table Data Settings @public */
    public generateSettings(): void {
        this.settings = {
            edit: {
                editButtonContent: '<i class="fa fa-edit" title="Edit"></i>',
                confirmSave: true
            },
            delete: {
                deleteButtonContent: '<i class="far fa-trash-alt" title="delete"></i>',
                confirmDelete: true
            },
            columns: this.columnLists,
            actions: {
                add: false,
                edit: false,
                delete: false,
                position: 'right'
            },
            attr: this.sharedService.tableClassConfig(),
            pager: this.sharedService.paginationPagerConfig(),
            noDataMessage: this.translateService.instant('NODATAMSG')
        };
    }

    /** smart table listing manipulation @public */
    public onChange(perPageValue: number): void {
        this.dataSource.setPaging(1, perPageValue, true);
    }

    /** OnUserRowSelect Function @public */
    public onUserRowSelect(event: MessageEvent): void {
        Object.assign(event.data, { page: 'ns-config-template' });
        this.dataService.changeMessage(event.data);
    }


    /** Generate nsData object from loop and return for the datasource @public */
    public generatetemplateData(nsData: NSCONFIG): NSConfigData {
        return {
            nsdId: nsData.nsdId,
            identifier: nsData._id,
            operationalState: nsData._admin.operationalState,
            created: this.sharedService.convertEpochTime(Number(nsData._admin.created)),
            modified: this.sharedService.convertEpochTime(Number(nsData._admin.modified)),
            name: nsData.name
        };
    }
    /** Handle compose new ns package method  @public */
    public composeTemplate(): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        const modalRef: NgbModalRef = this.modalService.open(ComposePackages, { backdrop: 'static' });
        modalRef.componentInstance.params = { page: 'ns-config-template' };
        modalRef.result.then((result: MODALCLOSERESPONSEDATA) => {
            if (result) {
                this.sharedService.callData();
            }
        }).catch((): void => {
            // Catch Navigation Error
        });
    }

    /**
     * Lifecyle hook which get trigger on component destruction
     */
    public ngOnDestroy(): void {
        this.generateDataSub.unsubscribe();
    }

    /** Fetching the data from server to Load in the smarttable @protected */
    protected generateData(): void {
        this.isLoadingResults = true;
        this.restService.getResource(environment.NSCONFIGTEMPLATE_URL).subscribe((nsdConfigData: NSCONFIG[]): void => {
            this.nsConfigData = [];
            nsdConfigData.forEach((nsddata: NSCONFIG): void => {
                const vnfDataObj: NSConfigData = this.generatetemplateData(nsddata);
                this.nsConfigData.push(vnfDataObj);
            });
            if (this.nsConfigData.length > 0) {
                this.checkDataClass = 'dataTables_present';
            } else {
                this.checkDataClass = 'dataTables_empty';
            }
            this.dataSource.load(this.nsConfigData).then((data: boolean): void => {
                this.isLoadingResults = false;
            }).catch((): void => {
                this.isLoadingResults = false;
            });
        }, (error: ERRORDATA): void => {
            this.restService.handleError(error, 'get');
            this.isLoadingResults = false;
        });
    }
}

/*
 Copyright 2020 TATA ELXSI

 Licensed under the Apache License, Version 2.0 (the 'License');
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: SANDHYA JS (sandhya.j@tataelxsi.co.in)
*/
/**
 * @file OKA Package details Component.
 */
import { Component, Injector, OnInit } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { CONFIGCONSTANT, ERRORDATA, MODALCLOSERESPONSEDATA } from 'CommonModel';
import { ComposePackages } from 'ComposePackages';
import { DataService } from 'DataService';
import { environment } from 'environment';
import { LocalDataSource } from 'ng2-smart-table';
import { OkaPackagesActionComponent } from 'OkaPackagesActionComponent';
import { RestService } from 'RestService';
import { Subscription } from 'rxjs';
import { SharedService } from 'SharedService';
import { VNFD, VNFData } from 'VNFDModel';

/**
 * Creating component
 * @Component takes OKAPackageComponent.html as template url
 */
@Component({
    selector: 'app-oka-packages',
    templateUrl: './OKAPackageComponent.html',
    styleUrls: ['./OKAPackageComponent.scss']
})
/** Exporting a class @exports OKAPackageComponent */
export class OKAPackageComponent implements OnInit {
    /** To inject services @public */
    public injector: Injector;

    /** Data of smarttable populate through LocalDataSource @public */
    public dataSource: LocalDataSource = new LocalDataSource();

    /** handle translate @public */
    public translateService: TranslateService;

    /** Columns list of the smart table @public */
    public columnLists: object = {};

    /** Settings for smarttable to populate the table with columns @public */
    public settings: object = {};

    /** operational State created data @public */
    public operationalStateFirstStep: string = CONFIGCONSTANT.k8OperationalStateFirstStep;

    /** operational State in creation data @public */
    public operationalStateSecondStep: string = CONFIGCONSTANT.k8OperationalStateStateSecondStep;

    /** operational State in deletion data @public */
    public operationalStateThirdStep: string = CONFIGCONSTANT.k8OperationalStateThirdStep;

    /** operational State failed deletion data @public */
    public operationalStateFourthStep: string = CONFIGCONSTANT.k8OperationalStateFourthStep;

    /** operational State failed creation data @public */
    public operationalStateFifthStep: string = CONFIGCONSTANT.k8OperationalStateFifthStep;

    /** Check the loading results @public */
    public isLoadingResults: boolean = true;

    /** Give the message for the loading @public */
    public message: string = 'PLEASEWAIT';

    /** Class for empty and present data @public */
    public checkDataClass: string;

    /** Instance of the rest service @private */
    private restService: RestService;

    /** dataService to pass the data from one component to another @private */
    private dataService: DataService;

    /** Formation of appropriate Data for LocalDatasource @private */
    private okaData: VNFData[] = [];

    /** Contains all methods related to shared @private */
    private sharedService: SharedService;

    /** Instance of the modal service @private */
    private modalService: NgbModal;

    /** Instance of subscriptions @private */
    private generateDataSub: Subscription;

    constructor(injector: Injector) {
        this.injector = injector;
        this.restService = this.injector.get(RestService);
        this.dataService = this.injector.get(DataService);
        this.sharedService = this.injector.get(SharedService);
        this.translateService = this.injector.get(TranslateService);
        this.modalService = this.injector.get(NgbModal);
    }

    /**
     * Lifecyle Hooks the trigger before component is instantiate
     */
    public ngOnInit(): void {
        this.generateColumns();
        this.generateSettings();
        this.generateData();
        this.generateDataSub = this.sharedService.dataEvent.subscribe((): void => { this.generateData(); });
    }

    /** smart table Header Colums @public */
    public generateColumns(): void {
        this.columnLists = {
            name: { title: this.translateService.instant('NAME'), width: '15%', sortDirection: 'asc' },
            identifier: { title: this.translateService.instant('IDENTIFIER'), width: '20%' },
            state: {
                title: this.translateService.instant('GITSTATE'), width: '15%', type: 'html',
                filter: {
                    type: 'list',
                    config: {
                        selectText: 'Select',
                        list: [
                            { value: this.operationalStateFirstStep, title: this.operationalStateFirstStep },
                            { value: this.operationalStateSecondStep, title: this.operationalStateSecondStep },
                            { value: this.operationalStateThirdStep, title: this.operationalStateThirdStep },
                            { value: this.operationalStateThirdStep, title: this.operationalStateFourthStep },
                            { value: this.operationalStateThirdStep, title: this.operationalStateFifthStep }
                        ]
                    }
                },
                valuePrepareFunction: (cell: VNFD, row: VNFD): string => {
                    if (row.state === this.operationalStateFirstStep) {
                        return `<span class="icon-label" title="${row.state}">
                                 <i class="fas fa-clock text-success"></i>
                                 </span>`;
                    } else if (row.state === this.operationalStateSecondStep) {
                        return `<span class="icon-label" title="${row.state}">
                                 <i class="fas fa-spinner text-warning"></i>
                                 </span>`;
                    } else if (row.state === this.operationalStateThirdStep) {
                        return `<span class="icon-label" title="${row.state}">
                                 <i class="fas fa-spinner text-danger"></i>
                                 </span>`;
                    } else if (row.state === this.operationalStateFourthStep) {
                        return `<span class="icon-label" title="${row.state}">
                                 <i class="fas fa-times-circle text-danger"></i>
                                 </span>`;
                    } else if (row.state === this.operationalStateFifthStep) {
                        return `<span class="icon-label" title="${row.state}">
                                 <i class="fas fa-times-circle text-warning"></i>
                                 </span>`;
                    } else {
                        return `<span>${row.state}</span>`;
                    }
                }
            },
            usageState: { title: this.translateService.instant('USAGESTATE'), width: '15%' },
            created: { title: this.translateService.instant('CREATED'), width: '15%' },
            Actions: {
                name: 'Action', width: '10%', filter: false, sort: false, type: 'custom',
                title: this.translateService.instant('ACTIONS'),
                valuePrepareFunction: (cell: VNFData, row: VNFData): VNFData => row, renderComponent: OkaPackagesActionComponent
            }
        };
    }

    /** smart table Data Settings @public */
    public generateSettings(): void {
        this.settings = {
            edit: {
                editButtonContent: '<i class="fa fa-edit" title="Edit"></i>',
                confirmSave: true
            },
            delete: {
                deleteButtonContent: '<i class="far fa-trash-alt" title="delete"></i>',
                confirmDelete: true
            },
            columns: this.columnLists,
            actions: {
                add: false,
                edit: false,
                delete: false,
                position: 'right'
            },
            attr: this.sharedService.tableClassConfig(),
            pager: this.sharedService.paginationPagerConfig(),
            noDataMessage: this.translateService.instant('NODATAMSG')
        };
    }

    /** smart table listing manipulation @public */
    public onChange(perPageValue: number): void {
        this.dataSource.setPaging(1, perPageValue, true);
    }

    /** OnUserRowSelect Function @public */
    public onUserRowSelect(event: MessageEvent): void {
        Object.assign(event.data, { page: 'oka-packages' });
        this.dataService.changeMessage(event.data);
    }

    /** Generate okaData object from loop and return for the datasource @public */
    public generateokaData(okadpackagedata: VNFD): VNFData {
        return {
            name: okadpackagedata.name,
            identifier: okadpackagedata._id,
            onboardingState: okadpackagedata._admin.onboardingState,
            usageState: okadpackagedata._admin.usageState,
            created: this.sharedService.convertEpochTime(Number(okadpackagedata._admin.created)),
            state: okadpackagedata.state
        };
    }
    /** Handle compose new oka package method  @public */
    public composeOKAPackage(): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        const modalRef: NgbModalRef = this.modalService.open(ComposePackages, { backdrop: 'static' });
        modalRef.componentInstance.params = { page: 'oka-packages', operationType: 'add'  };
        modalRef.result.then((result: MODALCLOSERESPONSEDATA) => {
            if (result) {
                this.sharedService.callData();
            }
        }).catch((): void => {
            // Catch Navigation Error
        });
    }

    /**
     * Lifecyle hook which get trigger on component destruction
     */
    public ngOnDestroy(): void {
        this.generateDataSub.unsubscribe();
    }

    /** Fetching the data from server to Load in the smarttable @protected */
    protected generateData(): void {
        this.isLoadingResults = true;
        this.restService.getResource(environment.OKAPACKAGES_URL).subscribe((okadPackageData: VNFD[]): void => {
            this.okaData = [];
            okadPackageData.forEach((okadpackagedata: VNFD): void => {
                const okaDataObj: VNFData = this.generateokaData(okadpackagedata);
                this.okaData.push(okaDataObj);
            });
            if (this.okaData.length > 0) {
                this.checkDataClass = 'dataTables_present';
            } else {
                this.checkDataClass = 'dataTables_empty';
            }
            this.dataSource.load(this.okaData).then((data: boolean): void => {
                this.isLoadingResults = false;
            }).catch((): void => {
                this.isLoadingResults = false;
            });
        }, (error: ERRORDATA): void => {
            this.restService.handleError(error, 'get');
            this.isLoadingResults = false;
        });
    }
}

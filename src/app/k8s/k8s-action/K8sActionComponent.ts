/*
 Copyright 2020 TATA ELXSI

 Licensed under the Apache License, Version 2.0 (the 'License');
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in  writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: KUMARAN M (kumaran.m@tataelxsi.co.in), RAJESH S (rajesh.s@tataelxsi.co.in), BARATH KUMAR R (barath.r@tataelxsi.co.in)
 */
/**
 * @file K8 Action Component
 */
import { HttpHeaders } from '@angular/common/http';
import { ChangeDetectorRef, Component, Injector } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { NotifierService } from 'angular-notifier';
import { ERRORDATA, GETAPIURLHEADER, MODALCLOSERESPONSEDATA } from 'CommonModel';
import { DeleteComponent } from 'DeleteComponent';
import { environment } from 'environment';
import { K8sAddClusterComponent } from 'K8sAddClusterComponent';
import { K8sAttachProfileComponent } from 'K8sAttachProfileComponent';
import { K8sInfraConfigAddComponent } from 'K8sInfraConfigAddComponent';
import { INFRACONFIGPAYLOAD, K8SCLUSTERDATADISPLAY, K8SPayload, K8SREPODATADISPLAY } from 'K8sModel';
import { KSUAddComponent } from 'KSUAddComponent';
import { RestService } from 'RestService';
import { isNullOrUndefined, SharedService } from 'SharedService';
import { ShowInfoComponent } from 'ShowInfoComponent';
/**
 * Creating component
 * @Component takes K8sActionComponent.html as template url
 */
@Component({
  selector: 'app-k8s-action',
  templateUrl: './K8sActionComponent.html',
  styleUrls: ['./K8sActionComponent.scss']
})
/** Exporting a class @exports K8sActionComponent */
export class K8sActionComponent {
  /** To inject services @public */
  public injector: Injector;

  /** To get the value from the Users action via valuePrepareFunction default Property of ng-smarttable @public */
  public value: K8SCLUSTERDATADISPLAY | K8SREPODATADISPLAY | INFRACONFIGPAYLOAD;

  /** handle translate @public */
  public translateService: TranslateService;

  /** Contains K8s Type @public */
  public getK8sType: string;

  /** Check register page @public */
  public checkRegister = false;

  /** Contains state @public */
  public state: string;

  /** Check profile or not @public */
  public isProfile = false;

  /** Check ksu or not @public */
  public isKSU = false;

  /** Check cluster or not @public */
  public isCluster = false;

  /** Check the loading results for loader status @public */
  public isLoadingDownloadResult: boolean = false;

  /** Instance of the modal service @private */
  private modalService: NgbModal;

  /** Contains all methods related to shared @private */
  private sharedService: SharedService;

  /** Contains instance ID @private */
  private instanceID: string;

  /** Utilizes rest service for any CRUD operations @private */
  private restService: RestService;

  /** Notifier service to popup notification @private */
  private notifierService: NotifierService;

  /** Detect changes for the User Input */
  private cd: ChangeDetectorRef;

  /** Set timeout @private */
  // eslint-disable-next-line @typescript-eslint/no-magic-numbers
  private timeOut: number = 1000;

  /** Controls the header form @private */
  private headers: HttpHeaders;

  constructor(injector: Injector) {
    this.injector = injector;
    this.modalService = this.injector.get(NgbModal);
    this.notifierService = this.injector.get(NotifierService);
    this.sharedService = this.injector.get(SharedService);
    this.translateService = this.injector.get(TranslateService);
    this.restService = this.injector.get(RestService);
    this.cd = this.injector.get(ChangeDetectorRef);
  }

  /**
   * Lifecyle Hooks the trigger before component is instantiate
   */
  public ngOnInit(): void {
    this.headers = new HttpHeaders({
      Accept: 'application/zip, application/json',
      'Cache-Control': 'no-cache, no-store, must-revalidate, max-age=0'
    });
    this.instanceID = this.value.identifier;
    this.getK8sType = this.value.pageType;
    this.state = this.value.state;
    if ((this.value.createdbyosm) === 'true') {
      this.isCluster = true;
    } else if ((this.value.createdbyosm) === 'false') {
      this.isCluster = false;
    }
    if (this.getK8sType === 'k8-ksu') {
      this.isKSU = true;
    } else {
      this.isKSU = false;
    }

    if (this.getK8sType === 'infra-config' || this.getK8sType === 'infra-controller' || this.getK8sType === 'app-profile' || this.getK8sType === 'resource-profile') {
    this.isProfile = true;
    } else {
      this.isProfile = false;
    }
  }

  /** Delete User Account @public */
  public deleteK8s(): void {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    const modalRef: NgbModalRef = this.modalService.open(DeleteComponent, { backdrop: 'static' });
    modalRef.result.then((result: MODALCLOSERESPONSEDATA) => {
      if (result) {
        this.sharedService.callData();
      }
    }).catch((): void => {
      // Catch Navigation Error
    });
  }

  /** Shows information using modalservice @public */
  public infoK8s(pageType: string): void {
    let pageName: string = '';
    let title: string = '';
    if (pageType === 'repo') {
      pageName = 'k8s-repo';
      title = 'PAGE.K8S.K8SREPODETAILS';
    } else {
      pageName = 'k8s-cluster';
      title = 'PAGE.K8S.K8SCLUSTERDETAILS';
    }
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    this.modalService.open(ShowInfoComponent, { backdrop: 'static' }).componentInstance.params = {
      id: this.instanceID,
      page: pageName,
      titleName: title,
      createdbyosm: this.value.createdbyosm,
      bootstrap: this.value.bootstrap,
      key: this.value.key
    };
  }
  /** Edit profile @public */
  public editProfile(editType: string): void {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    const modalRef: NgbModalRef = this.modalService.open(K8sInfraConfigAddComponent, { backdrop: 'static' });
    modalRef.componentInstance.profileID = this.value.identifier;
    modalRef.componentInstance.profileType = editType;
    modalRef.componentInstance.profileName = this.value.name;
    modalRef.componentInstance.profileDescription = this.value.description;
    modalRef.result.then((result: MODALCLOSERESPONSEDATA) => {
      if (result) {
        this.sharedService.callData();
      }
    }).catch((): void => {
      // Catch Navigation Error
    });
  }

  /** Edit cluster @public */
  public editCluster(editType: string): void {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    const modalRef: NgbModalRef = this.modalService.open(K8sAddClusterComponent, { backdrop: 'static' });
    modalRef.componentInstance.profileID = this.value.identifier;
    modalRef.componentInstance.profileType = editType;
    modalRef.result.then((result: MODALCLOSERESPONSEDATA) => {
      if (result) {
        this.sharedService.callData();
      }
    }).catch((): void => {
      // Catch Navigation Error
    });
  }


  /** Edit profile under cluster @public */
  public editClusterProfile(editType: string): void {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    const modalRef: NgbModalRef = this.modalService.open(K8sAttachProfileComponent, { backdrop: 'static' });
    modalRef.componentInstance.profileID = this.value.identifier;
    modalRef.componentInstance.profileType = editType;
    modalRef.result.then((result: MODALCLOSERESPONSEDATA) => {
      if (result) {
        this.sharedService.callData();
      }
    }).catch((): void => {
      // Catch Navigation Error
    });
  }

  /** Move KSU @public */
  public moveKsu(): void {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    const modalRef: NgbModalRef = this.modalService.open(KSUAddComponent, { backdrop: 'static' });
    modalRef.componentInstance.profileID = this.value.identifier;
    modalRef.componentInstance.profileType = 'move';
    modalRef.result.then((result: MODALCLOSERESPONSEDATA) => {
      if (result) {
        this.sharedService.callData();
      }
    }).catch((): void => {
      // Catch Navigation Error
    });
  }

  /** Edit KSU @public */
  public editKsu(): void {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    const modalRef: NgbModalRef = this.modalService.open(KSUAddComponent, { backdrop: 'static' });
    modalRef.componentInstance.profileID = this.value.identifier;
    modalRef.componentInstance.profileType = 'edit';
    modalRef.result.then((result: MODALCLOSERESPONSEDATA) => {
      if (result) {
        this.sharedService.callData();
      }
    }).catch((): void => {
      // Catch Navigation Error
    });
  }

  /** Download credentials @public */
  public getCredentials(): void {
    this.isLoadingDownloadResult = true;
    const httpOptions: GETAPIURLHEADER = {
      headers: this.headers,
      responseType: 'blob'
    };
    this.restService.getResource(environment.K8SCREATECLUSTER_URL + '/' + this.instanceID + '/get_creds')
      .subscribe((res: { op_id: string }) => {
        if (!isNullOrUndefined(res.op_id)) {
          this.restService.getResource(environment.K8SCREATECLUSTER_URL + '/' + this.instanceID + '/get_creds_file' + '/' + res.op_id, httpOptions)
            .subscribe((response: Blob) => {
              this.isLoadingDownloadResult = true;
              if (!isNullOrUndefined(response)) {
                this.isLoadingDownloadResult = false;
                const binaryData: Blob[] = [];
                binaryData.push(response);
                this.sharedService.downloadFiles(this.value.name, binaryData, response.type);
                this.isLoadingDownloadResult = false;
                this.changeDetactionforDownload();
              }
            }, (error: ERRORDATA) => {
              this.isLoadingDownloadResult = false;
              this.notifierService.notify('error', this.translateService.instant('ERROR'));
              this.changeDetactionforDownload();
              if (typeof error.error === 'object') {
                error.error.text().then((data: string): void => {
                  error.error = JSON.parse(data);
                  this.restService.handleError(error, 'getBlob');
                });
              }
            });
        }
      }, (error: ERRORDATA) => {
        this.isLoadingDownloadResult = false;
        this.restService.handleError(error, 'get');
      });
  }

  /** Clone KSU @public */
  public cloneKsu(): void {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    const modalRef: NgbModalRef = this.modalService.open(KSUAddComponent, { backdrop: 'static' });
    modalRef.componentInstance.profileID = this.value.identifier;
    modalRef.componentInstance.profileType = 'clone';
    modalRef.result.then((result: MODALCLOSERESPONSEDATA) => {
      if (result) {
        this.sharedService.callData();
      }
    }).catch((): void => {
      // Catch Navigation Error
    });
  }

  /** Change the detaction @public */
  public changeDetactionforDownload(): void {
    setTimeout(() => {
      this.cd.detectChanges();
    }, this.timeOut);
  }
}

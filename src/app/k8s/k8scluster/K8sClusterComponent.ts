/*
 Copyright 2020 TATA ELXSI

 Licensed under the Apache License, Version 2.0 (the 'License');
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: KUMARAN M (kumaran.m@tataelxsi.co.in), RAJESH S (rajesh.s@tataelxsi.co.in), BARATH KUMAR R (barath.r@tataelxsi.co.in)
*/
/**
 * @file k8sclustercomponent.ts.
 */
import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { CONFIGCONSTANT, ERRORDATA, MODALCLOSERESPONSEDATA } from 'CommonModel';
import { DataService } from 'DataService';
import { environment } from 'environment';
import { K8sActionComponent } from 'K8sActionComponent';
import { K8sAddClusterComponent } from 'K8sAddClusterComponent';
import { K8SCLUSTERDATA, K8SCLUSTERDATADISPLAY, K8SCREATEDATADISPLAY } from 'K8sModel';
import { LocalDataSource } from 'ng2-smart-table';
import { RestService } from 'RestService';
import { forkJoin, Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { isNullOrUndefined, SharedService } from 'SharedService';
/**
 * Creating Component
 * @Component takes K8sClusterComponent.html as template url
 */
@Component({
  selector: 'app-k8scluster',
  templateUrl: './K8sClusterComponent.html',
  styleUrls: ['./K8sClusterComponent.scss']
})
/** Exporting a class @exports K8sClusterComponent */
export class K8sClusterComponent implements OnInit, OnDestroy {
  /** To inject services @public */
  public injector: Injector;

  /** handle translate @public */
  public translateService: TranslateService;

  /** Data of smarttable populate through LocalDataSource @public */
  public dataSource: LocalDataSource = new LocalDataSource();

  /** Columns list of the smart table @public */
  public columnList: object = {};

  /** Settings for smarttable to populate the table with columns @public */
  public settings: object = {};

  /** Check the loading results @public */
  public isLoadingResults: boolean = true;

  /** Give the message for the loading @public */
  public message: string = 'PLEASEWAIT';

  /** Class for empty and present data @public */
  public checkDataClass: string;

  /** operational State created data @public */
  public operationalStateFirstStep: string = CONFIGCONSTANT.k8OperationalStateFirstStep;

  /** operational State in creation data @public */
  public operationalStateSecondStep: string = CONFIGCONSTANT.k8OperationalStateStateSecondStep;

  /** operational State in deletion data @public */
  public operationalStateThirdStep: string = CONFIGCONSTANT.k8OperationalStateThirdStep;

  /** operational State failed deletion data @public */
  public operationalStateFourthStep: string = CONFIGCONSTANT.k8OperationalStateFourthStep;

  /** operational State failed creation data @public */
  public operationalStateFifthStep: string = CONFIGCONSTANT.k8OperationalStateFifthStep;

  /** operational State failed creation data @public */
  public operationalStateSixthStep: string = CONFIGCONSTANT.k8OperationalStateSixthStep;

  /** Cluster Mode Managed data @public */
  public clusterModeFirstStep: string = CONFIGCONSTANT.clusterModeFirstStep;

  /** Cluster Mode Regsitered data @public */
  public clusterModeSecondStep: string = CONFIGCONSTANT.clusterModeSecondStep;

  /** cluster Type @public */
  public isCluster: string = 'Registered';

  /** cluster  @public */
  public clusterUrl: string;

  /** Instance of the rest service @private */
  private restService: RestService;

  /** dataService to pass the data from one component to another @private */
  private dataService: DataService;

  /** Formation of appropriate Data for LocalDatasource @private */
  private k8sClusterData: {}[] = [];

  /** Contains all methods related to shared @private */
  private sharedService: SharedService;

  /** Instance of the modal service @private */
  private modalService: NgbModal;

  /** Instance of subscriptions @private */
  private generateDataSub: Subscription;

  constructor(injector: Injector) {
    this.injector = injector;
    this.restService = this.injector.get(RestService);
    this.dataService = this.injector.get(DataService);
    this.sharedService = this.injector.get(SharedService);
    this.translateService = this.injector.get(TranslateService);
    this.modalService = this.injector.get(NgbModal);
  }
  /** Lifecyle Hooks the trigger before component is instantiate @public */
  public ngOnInit(): void {
    this.generateColumns();
    this.generateSettings();
    this.generateData();
    this.generateDataSub = this.sharedService.dataEvent.subscribe(() => { this.generateData(); });
    sessionStorage.setItem('clusterType', this.isCluster);
  }

  /** smart table Header Colums @public */
  public generateColumns(): void {
    this.columnList = {
      name: { title: this.translateService.instant('NAME'), width: '20%', sortDirection: 'asc' },
      identifier: { title: this.translateService.instant('IDENTIFIER'), width: '20%' },
      version: { title: this.translateService.instant('K8VERSION'), width: '10%' },
      clusterMode: {
        title: this.translateService.instant('Cluster Mode'), width: '15%', type: 'html',
        filter: {
          type: 'list',
          config: {
            selectText: 'Select',
            list: [
              { value: this.clusterModeFirstStep, title: this.clusterModeFirstStep },
              { value: this.clusterModeSecondStep, title: this.clusterModeSecondStep }
            ]
          }
        },
        valuePrepareFunction: (cell: K8SCLUSTERDATADISPLAY, row: K8SCLUSTERDATADISPLAY): string => {
          if (row.clusterMode === this.clusterModeFirstStep) {
            return `<span class="icon-label" title="${row.clusterMode}">
                         <i class="fas fa-cloud-upload-alt text-info"></i>
                         </span>`;
          } else if (row.clusterMode === this.clusterModeSecondStep) {
            return `<span class="icon-label" title="${row.clusterMode}">
                         <i class="fas fa-clipboard-check text-info"></i>
                         </span>`;
          } else {
            return `<span>${row.clusterMode}</span>`;
          }
        }
      },
      state: {
        title: this.translateService.instant('GITSTATE'), width: '15%', type: 'html',
        filter: {
          type: 'list',
          config: {
            selectText: 'Select',
            list: [
              { value: this.operationalStateFirstStep, title: this.operationalStateFirstStep },
              { value: this.operationalStateSecondStep, title: this.operationalStateSecondStep },
              { value: this.operationalStateThirdStep, title: this.operationalStateThirdStep },
              { value: this.operationalStateFourthStep, title: this.operationalStateFourthStep },
              { value: this.operationalStateFifthStep, title: this.operationalStateFifthStep },
              { value: this.operationalStateSixthStep, title: this.operationalStateFifthStep }
            ]
          }
        },
        valuePrepareFunction: (cell: K8SCLUSTERDATADISPLAY, row: K8SCLUSTERDATADISPLAY): string => {
          if (row.state === this.operationalStateFirstStep) {
            return `<span class="icon-label" title="${row.state}">
                         <i class="fas fa-clock text-success"></i>
                         </span>`;
          } else if (row.state === this.operationalStateSecondStep) {
            return `<span class="icon-label" title="${row.state}">
                         <i class="fas fa-spinner text-warning"></i>
                         </span>`;
          } else if (row.state === this.operationalStateThirdStep) {
            return `<span class="icon-label" title="${row.state}">
                         <i class="fas fa-spinner text-danger"></i>
                         </span>`;
          } else if (row.state === this.operationalStateFourthStep) {
            return `<span class="icon-label" title="${row.state}">
                         <i class="fas fa-times-circle text-danger"></i>
                         </span>`;
          } else if (row.state === this.operationalStateFifthStep) {
            return `<span class="icon-label" title="${row.state}">
                         <i class="fas fa-times-circle text-warning"></i>
                         </span>`;
          } else if (row.state === this.operationalStateSixthStep) {
            return `<span class="icon-label" title="${row.state}">
                         <i class="fas fa-ban text-danger"></i>
                         </span>`;
          } else {
            return `<span>${row.state}</span>`;
          }
        }
      },
      created: { title: this.translateService.instant('CREATED'), width: '15%' },
      Actions: {
        name: 'Action', width: '5%', filter: false, sort: false, title: this.translateService.instant('ACTIONS'), type: 'custom',
        valuePrepareFunction: (cell: K8SCLUSTERDATADISPLAY, row: K8SCLUSTERDATADISPLAY): K8SCLUSTERDATADISPLAY => row,
        renderComponent: K8sActionComponent
      }
    };
  }

  /** smart table Data Settings @public */
  public generateSettings(): void {
    this.settings = {
      columns: this.columnList,
      actions: { add: false, edit: false, delete: false, position: 'right' },
      attr: this.sharedService.tableClassConfig(),
      pager: this.sharedService.paginationPagerConfig(),
      noDataMessage: this.translateService.instant('NODATAMSG')
    };
  }

  /** smart table listing manipulation @public */
  public onChange(perPageValue: number): void {
    this.dataSource.setPaging(1, perPageValue, true);
  }

  /** smart table listing manipulation @public */
  public onUserRowSelect(event: MessageEvent): void {
    Object.assign(event.data, { page: 'k8-cluster' });
    this.dataService.changeMessage(event.data);
  }

  /** Compose new K8s Cluster Accounts @public */
  public addK8sCluster(type?: string): void {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    const modalRef: NgbModalRef = this.modalService.open(K8sAddClusterComponent, { backdrop: 'static' });

    if (type === 'Register') {
      modalRef.componentInstance.profileType = 'Register';
    } else {
      modalRef.componentInstance.profileType = 'Manage';
    }
    modalRef.result.then((result: MODALCLOSERESPONSEDATA) => {
      if (result) {
        this.sharedService.callData();
      }
    }).catch((): void => {
      // Catch Navigation Error
    });
  }

  /**
   * Lifecyle hook which get trigger on component destruction
   */
  public ngOnDestroy(): void {
    this.generateDataSub.unsubscribe();
  }

  /** Generate nsData object from loop and return for the datasource @public */
  public generateK8sclusterData(k8sClusterdata: K8SCLUSTERDATA): K8SCLUSTERDATADISPLAY {
    return {
      name: k8sClusterdata.name,
      state: !isNullOrUndefined(k8sClusterdata.state) ? k8sClusterdata.state : 'N/A',
      identifier: k8sClusterdata._id,
      operationalState: !isNullOrUndefined(k8sClusterdata._admin.operationalState) ? k8sClusterdata._admin.operationalState : 'N/A',
      version: k8sClusterdata.k8s_version,
      created: this.sharedService.convertEpochTime(Number(k8sClusterdata._admin.created)),
      modified: this.sharedService.convertEpochTime(Number(k8sClusterdata._admin.modified)),
      pageType: 'cluster',
      createdbyosm: !isNullOrUndefined(k8sClusterdata.created) ? (k8sClusterdata.created) : 'false',
      bootstrap: !isNullOrUndefined(k8sClusterdata.bootstrap) ? (k8sClusterdata.bootstrap) : false,
      key: (k8sClusterdata.key === 'registered') ? true : false,
      clusterMode: (k8sClusterdata.created === 'true') ? 'MANAGED' : 'REGISTERED'
    };
  }

  /** Fetching the data from server to Load in the smarttable @protected */
  protected generateData(): void {
    this.isLoadingResults = true;
    this.restService.getResource(environment.K8SCREATECLUSTER_URL).subscribe((k8sClusterDatas: K8SCLUSTERDATA[]) => {
      if (!isNullOrUndefined(k8sClusterDatas)) {
        this.k8sClusterData = [];
        k8sClusterDatas.forEach((clusterData: K8SCLUSTERDATA) => {
          const k8sClusterDataObj: K8SCLUSTERDATADISPLAY = this.generateK8sclusterData(clusterData);
          this.k8sClusterData.push(k8sClusterDataObj);
        });
        if (this.k8sClusterData.length > 0) {
          this.checkDataClass = 'dataTables_present';
        } else {
          this.checkDataClass = 'dataTables_empty';
        }
        this.dataSource.load(this.k8sClusterData).then((data: boolean) => {
          this.isLoadingResults = false;
        }).catch(() => {
          this.isLoadingResults = false;
        });
      }
    }, (error: ERRORDATA) => {
      this.restService.handleError(error, 'get');
      this.isLoadingResults = false;
    });
  }
}

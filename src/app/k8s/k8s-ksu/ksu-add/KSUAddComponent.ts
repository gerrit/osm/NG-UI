/*
 Copyright 2020 TATA ELXSI

 Licensed under the Apache License, Version 2.0 (the 'License');
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: SANDHYA JS (sandhya.j@tataelxsi.co.in)
*/
/**
 * @file KSUAddComponent.ts.
 */
import { HttpHeaders } from '@angular/common/http';
import { Component, ElementRef, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { NotifierService } from 'angular-notifier';
import { APIURLHEADER, ERRORDATA, MODALCLOSERESPONSEDATA, TYPESECTION, URLPARAMS } from 'CommonModel';
import { environment } from 'environment';
import * as jsyaml from 'js-yaml';
import { INFRACONFIGPAYLOAD, KSU, OKA } from 'K8sModel';
import { RestService } from 'RestService';
import { isNullOrUndefined, SharedService } from 'SharedService';
/**
 * Creating Component
 * @Component takes KSUAddComponent.html as template url
 */
@Component({
    selector: 'app-ksu-add',
    templateUrl: './KSUAddComponent.html',
    styleUrls: ['./KSUAddComponent.scss']
})
/** Exporting a class @exports KSUAddComponent */
export class KSUAddComponent implements OnInit {
    /** To inject services @public */
    public injector: Injector;

    /** FormGroup instance added to the form @ html @public */
    public KsuForm: FormGroup;

    /** Contains all deployment methods */
    public profileSelect: TYPESECTION[] = [];

    /** Instance for active modal service @public */
    public activeModal: NgbActiveModal;

    /** Form submission Add */
    public submitted: boolean = false;

    /** check move action */
    public isMove: boolean = false;

    /** check clone action */
    public isClone: boolean = false;

    /** check KSU */
    public isKsu: boolean = false;

    /** Check the loading results @public */
    public isLoadingResults: boolean = false;

    /** Give the message for the loading @public */
    public message: string = 'PLEASEWAIT';

    /** contains url @public */
    public profileUrl: string;

    /** contains profileData @public */
    public profileData: {}[] = [];

    /** OKA array @private */
    private okaArray: FormArray;

    /** contains profile details @public */
    public profileDetails: {}[];

    /** contains Oka Data @public */
    public okaData: {}[] = [];

    /** contains OKA details @public */
    public okaDetail: {}[];

    /** contains payload @public */
    public payload: INFRACONFIGPAYLOAD | KSU;

    /** oka Form array @private */
    private okaFormArray: FormArray;

    /** Input contains Modal dialog component Instance @public */
    @Input() public profileType: string;

    /** Input contains Modal dialog component Instance @public */
    @Input() public profileID: string;

    /** Element ref for fileInputConfig @public */
    @ViewChild('fileInputConfig') fileInputConfig: ElementRef<HTMLInputElement>;

    /** Element ref for fileInputConfigLabel @public */
    @ViewChild('fileInputConfigLabel') fileInputConfigLabel: ElementRef<HTMLLabelElement>;

    /** FormBuilder instance added to the formBuilder @private */
    private formBuilder: FormBuilder;

    /** Utilizes rest service for any CRUD operations @private */
    private restService: RestService;

    /** Notifier service to popup notification @private */
    private notifierService: NotifierService;

    /** Contains tranlsate instance @private */
    private translateService: TranslateService;

    /** Controls the header form @private */
    private headers: HttpHeaders;

    /** Contains all methods related to shared @private */
    private sharedService: SharedService;

    constructor(injector: Injector) {
        this.injector = injector;
        this.restService = this.injector.get(RestService);
        this.activeModal = this.injector.get(NgbActiveModal);
        this.formBuilder = this.injector.get(FormBuilder);
        this.notifierService = this.injector.get(NotifierService);
        this.translateService = this.injector.get(TranslateService);
        this.sharedService = this.injector.get(SharedService);
        this.profileSelect = [
            {
                title: 'Infra Config Profile',
                value: 'infra_config_profiles'
            },
            {
                title: 'Infra Controller Profile',
                value: 'infra_controller_profiles'
            }, {
                title: 'App Profile',
                value: 'app_profiles'
            }, {
                title: 'Resource Profile',
                value: 'resource_profiles'
            }
        ];
    }

    public ngOnInit(): void {
        /** On Initializing call the methods */
        this.KsuFormAction();
        this.headers = new HttpHeaders({
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache, no-store, must-revalidate, max-age=0'
        });
        this.okaDetails();
        if (this.profileType === 'clone') {
            this.isClone = true;
        } else if (this.profileType === 'move') {
            this.isMove = true;
        } else if (this.profileType === 'edit') {
            this.isKsu = true;
            this.getksuDetails();
        } else {
            this.isKsu = true;
        }
    }

    /** Generate primitive params @public */
    get okaParamsBuilder(): FormGroup {
        return this.formBuilder.group({
            _id: [null],
            sw_catalog_path: [null],
            transformation: [null]
        });
    }


    /** On modal initializing forms  @public */
    public KsuFormAction(): void {
        this.KsuForm = this.formBuilder.group({
            name: ['', [Validators.required]],
            description: ['', [Validators.required]],
            profile_type: [null, [Validators.required]],
            id: [null, [Validators.required]],
            oka: this.formBuilder.array([])
        });
    }

    /** Handle FormArray Controls @public */
    public getControls(): AbstractControl[] {
        return (this.KsuForm.get('oka') as FormArray).controls;
    }

    /** convenience getter for easy access to form fields */
    get f(): FormGroup['controls'] { return this.KsuForm.controls; }

    /** Get OKA details @public */
    public okaDetails(): void {
        this.isLoadingResults = true;
        this.restService.getResource(environment.OKAPACKAGES_URL)
            .subscribe((okaData: []): void => {
                okaData.forEach((okaDetail: INFRACONFIGPAYLOAD): void => {
                    const oka: {} = {
                        name: okaDetail.name,
                        id: okaDetail._id
                    };
                    this.okaData.push(oka);
                });
                this.okaDetail = this.okaData;
                this.isLoadingResults = false;
            }, (error: ERRORDATA): void => {
                this.isLoadingResults = false;
                this.restService.handleError(error, 'get');
            });
    }

    /** Get KSU details @public */
    public getksuDetails(): void {
        this.restService.getResource(environment.KSU_URL + '/' + this.profileID)
            .subscribe((ksuData: KSU): void => {
                this.KsuForm.patchValue({ id: ksuData.profile._id, name: ksuData.name, description: ksuData.description, profile_type: ksuData.profile.profile_type });
                this.getprofileDetails(this.KsuForm.value.profile_type, ksuData.profile.name);
                ksuData.oka.forEach((data: OKA): void => {
                    this.okaArray = this.KsuForm.get('oka') as FormArray;
                    const transformations: string = JSON.stringify(data.transformation);
                    if (!isNullOrUndefined(data._id)) {
                        data.sw_catalog_path = null;
                    } else if (!isNullOrUndefined(data.sw_catalog_path)) {
                        data._id = null;
                    }
                    const okaFormGroup = this.formBuilder.group({
                        _id: data._id,
                        sw_catalog_path: data.sw_catalog_path,
                        transformation: transformations
                    });
                    this.okaArray.push(okaFormGroup);
                });
            }, (error: ERRORDATA): void => {
                this.restService.handleError(error, 'get');
            });
    }


    /** On modal submit ksuAddSubmit will called @public */
    public KSUAddSubmit(): void {
        if (this.profileType === 'move') {
            this.profileUrl = environment.KSU_URL + '/' + this.profileID + '/move';
            this.payload = {
                profile: {
                    _id: this.KsuForm.value.id,
                    profile_type: this.KsuForm.value.profile_type
                }
            };
            this.getFormControl('description').disable();
            this.getFormControl('oka').disable();
            this.getFormControl('name').disable();
        } else if (this.profileType === 'clone') {
            this.getFormControl('description').disable();
            this.getFormControl('oka').disable();
            this.profileUrl = environment.KSU_URL + '/' + this.profileID + '/clone';
            this.payload = {
                name: this.KsuForm.value.name,
                profile: {
                    _id: this.KsuForm.value.id,
                    profile_type: this.KsuForm.value.profile_type
                }
            };
        } else if (this.profileType === 'edit' || this.profileType === 'add') {
            this.addEditKSU();
        }
        this.submitted = true;
        this.sharedService.cleanForm(this.KsuForm);
        if (this.KsuForm.invalid) {
            return;
        }
        if (this.profileType === 'edit') {
            this.editKSU();
        } else {
            this.addKSU();
        }
    }

    /** Add/Edit KSU @public */
    public addEditKSU(): void {
        this.okaArray = this.KsuForm.get('oka') as FormArray;
        const transValue = this.okaArray.at(0).get('transformation').value;
        const validJSON: boolean = this.sharedService.checkJson(transValue);
        if (validJSON) {
            for (let i = 0; i < this.okaArray.length; i++) {
                const formGroup = this.okaArray.at(i) as FormGroup;
                formGroup.patchValue({
                    transformation: JSON.parse(transValue)
                });
            }
        } else {
            const getConfigJson: string = jsyaml.load(transValue, { json: true });
            for (let i = 0; i < this.okaArray.length; i++) {
                const formGroup = this.okaArray.at(i) as FormGroup;
                formGroup.patchValue({
                    transformation: JSON.parse(getConfigJson)
                });
            }
        }
        if (this.profileType === 'edit') {
            this.profileUrl = environment.KSU_URL + '/' + this.profileID;
            this.payload = {
                name: this.KsuForm.value.name,
                description: this.KsuForm.value.description,
                profile: {
                    _id: this.KsuForm.value.id,
                    sw_catalog_path: this.KsuForm.value.sw_catalog_path,
                    profile_type: this.KsuForm.value.profile_type
                },
                oka: this.KsuForm.value.oka
            };
        } else {
            this.profileUrl = environment.KSU_URL;
            this.payload = {
                ksus: [{
                    name: this.KsuForm.value.name,
                    description: this.KsuForm.value.description,
                    profile: {
                        _id: this.KsuForm.value.id,
                        sw_catalog_path: this.KsuForm.value.sw_catalog_path,
                        profile_type: this.KsuForm.value.profile_type
                    },
                    oka: this.KsuForm.value.oka
                }]
            };
        }
    }
    /** Add/move/clone KSU @public */
    public addKSU(): void {
        this.isLoadingResults = true;
        const modalData: MODALCLOSERESPONSEDATA = {
            message: 'Done'
        };
        const apiURLHeader: APIURLHEADER = {
            url: this.profileUrl,
            httpOptions: { headers: this.headers }
        };
        this.restService.postResource(apiURLHeader, this.payload).subscribe((result: {}) => {
            this.activeModal.close(modalData);
            this.isLoadingResults = false;
            if (this.profileType === 'move') {
                this.notifierService.notify('success',
                    this.translateService.instant('PAGE.K8S.MOVEDSUCCESSFULLY'));
            } else if (this.profileType === 'clone') {
                this.notifierService.notify('success',
                    this.translateService.instant('PAGE.K8S.CLONEDSUCCESSFULLY'));
            } else {
                this.notifierService.notify('success',
                    this.translateService.instant('PAGE.K8S.KSUCREATEDSUCCESSFULLY'));
            }
        }, (error: ERRORDATA) => {
            this.restService.handleError(error, 'post');
            // eslint-disable-next-line @typescript-eslint/no-magic-numbers
            if (error.error.status === 422 || error.error.status === 400) {
                this.activeModal.close(modalData);
            }
            this.isLoadingResults = false;
        });
    }

    /** Edit KSU @public */
    public editKSU(): void {
        this.isLoadingResults = true;
        const modalData: MODALCLOSERESPONSEDATA = {
            message: 'Done'
        };
        const apiURLHeader: APIURLHEADER = {
            url: this.profileUrl,
            httpOptions: { headers: this.headers }
        };
        this.restService.patchResource(apiURLHeader, this.payload).subscribe((result: {}) => {
            this.activeModal.close(modalData);
            this.isLoadingResults = false;
            this.notifierService.notify('success',
                this.translateService.instant('PAGE.K8S.KSUEDITEDSUCCESSFULLY'));
        }, (error: ERRORDATA) => {
            this.restService.handleError(error, 'post');
            // eslint-disable-next-line @typescript-eslint/no-magic-numbers
            if (error.error.status === 422 || error.error.status === 400) {
                this.activeModal.close(modalData);
            }
            this.isLoadingResults = false;
        });
    }

    /** Config file process @private */
    public configFile(files: FileList): void {
        if (files && files.length === 1) {
            const fileFormat: string = this.sharedService.fetchFileExtension(files).toLocaleLowerCase();
            if (fileFormat === 'yaml' || fileFormat === 'yml') {
                this.sharedService.getFileString(files, 'yaml').then((fileContent: string): void => {
                    this.okaArray = this.KsuForm.get('oka') as FormArray;
                    for (let i = 0; i < this.okaArray.length; i++) {
                        const formGroup = this.okaArray.at(i) as FormGroup;
                        formGroup.patchValue({
                            transformation: fileContent // Set the value for the specified key
                        });
                    }
                }).catch((err: string): void => {
                    if (err === 'typeError') {
                        this.notifierService.notify('error', this.translateService.instant('YAMLFILETYPEERRROR'));
                    } else {
                        this.notifierService.notify('error', this.translateService.instant('ERROR'));
                    }
                    this.fileInputConfigLabel.nativeElement.innerText = this.translateService.instant('CHOOSEFILE');
                    this.fileInputConfig.nativeElement.value = null;
                });
            } else if (fileFormat === 'json') {
                this.sharedService.getFileString(files, 'json').then((fileContent: string): void => {
                    const getConfigJson: string = jsyaml.load(fileContent, { json: true });
                    this.KsuForm.get('transformation').setValue(JSON.stringify(getConfigJson));
                }).catch((err: string): void => {
                    if (err === 'typeError') {
                        this.notifierService.notify('error', this.translateService.instant('JSONFILETYPEERRROR'));
                    } else {
                        this.notifierService.notify('error', this.translateService.instant('ERROR'));
                    }
                    this.fileInputConfigLabel.nativeElement.innerText = this.translateService.instant('CHOOSEFILE');
                    this.fileInputConfig.nativeElement.value = null;
                });
            }
        } else if (files && files.length > 1) {
            this.notifierService.notify('error', this.translateService.instant('DROPFILESVALIDATION'));
        }
        this.fileInputConfigLabel.nativeElement.innerText = files[0].name;
        this.fileInputConfig.nativeElement.value = null;
    }
    /** Add OKA @public */
    public addOka(): void {
        this.okaFormArray = this.KsuForm.get('oka') as FormArray;
        this.okaFormArray.push(this.okaParamsBuilder);
    }

    /** Remove OKA @public */
    public removeMapping(index: number): void {
        this.okaFormArray.removeAt(index);
    }

    /** Get profile details @public */
    public getprofileDetails(event: string, name?: string): void {
        this.profileData = [];
        if (event === 'infra_config_profiles') {
            this.profileUrl = environment.K8SINFRACONFIGPROFILE_URL;
        } else if (event === 'infra_controller_profiles') {
            this.profileUrl = environment.K8SINFRACONTROLLERPROFILE_URL;
        } else if (event === 'app_profiles') {
            this.profileUrl = environment.K8SAPPPROFILE_URL;
        } else if (event === 'resource_profiles') {
            this.profileUrl = environment.K8SRESOURCEPROFILE_URL;
        }
        this.restService.getResource(this.profileUrl)
            .subscribe((profileData: []): void => {
                profileData.forEach((profileDetail: INFRACONFIGPAYLOAD): void => {
                    const profile: {} = {
                        name: profileDetail.name,
                        id: profileDetail._id
                    };
                    this.profileData.push(profile);
                });
                this.profileDetails = this.profileData;
            }, (error: ERRORDATA): void => {
                this.restService.handleError(error, 'get');
            });
    }
    /** Used to get the AbstractControl of controlName passed @private */
    private getFormControl(controlName: string): AbstractControl {
        // eslint-disable-next-line security/detect-object-injection
        return this.KsuForm.controls[controlName];
    }
}

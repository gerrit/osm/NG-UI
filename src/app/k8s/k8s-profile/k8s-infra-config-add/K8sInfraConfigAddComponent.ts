/*
 Copyright 2020 TATA ELXSI

 Licensed under the Apache License, Version 2.0 (the 'License');
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: SANDHYA JS (sandhya.j@tataelxsi.co.in)
*/
/**
 * @file K8sInfraConfigAddComponent.ts.
 */
import { HttpHeaders } from '@angular/common/http';
import { Component, Injector, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { NotifierService } from 'angular-notifier';
import { APIURLHEADER, ERRORDATA, MODALCLOSERESPONSEDATA, TYPESECTION, URLPARAMS } from 'CommonModel';
import { environment } from 'environment';
import { INFRACONFIGPAYLOAD } from 'K8sModel';
import { RestService } from 'RestService';
import { SharedService, isNullOrUndefined } from 'SharedService';
/**
 * Creating Component
 * @Component takes K8sInfraConfigAddComponent.html as template url
 */
@Component({
    selector: 'app-infra-config',
    templateUrl: './K8sInfraConfigAddComponent.html',
    styleUrls: ['./K8sInfraConfigAddComponent.scss']
})
/** Exporting a class @exports K8sInfraConfigAddComponent */
export class K8sInfraConfigAddComponent implements OnInit {
    /** To inject services @public */
    public injector: Injector;

    /** FormGroup instance added to the form @ html @public */
    public profileForm: FormGroup;

    /** Instance for active modal service @public */
    public activeModal: NgbActiveModal;

    /** Form submission Add */
    public submitted: boolean = false;

    /** Check the loading results @public */
    public isLoadingResults: boolean = false;

    /** Give the message for the loading @public */
    public message: string = 'PLEASEWAIT';

    /** profile url @public */
    public profileUrl: string;

    /** Input contains Modal dialog component Instance @public */
    @Input() public profileType: string;

    /** Input contains Modal dialog component Instance @public */
    @Input() public profileID: string;

    /** Input contains Modal dialog component Instance @public */
    @Input() public profileName: string;

    /** Input contains Modal dialog component Instance @public */
    @Input() public profileDescription: string;

    /** check Add @public */
    public isAdd = false;

    /** check Edit @public */
    public isEdit = false;

    /** Edit Payload @public */
    public payload: INFRACONFIGPAYLOAD;

    /** FormBuilder instance added to the formBuilder @private */
    private formBuilder: FormBuilder;

    /** Utilizes rest service for any CRUD operations @private */
    private restService: RestService;

    /** Notifier service to popup notification @private */
    private notifierService: NotifierService;

    /** Contains tranlsate instance @private */
    private translateService: TranslateService;

    /** Controls the header form @private */
    private headers: HttpHeaders;

    /** Contains all methods related to shared @private */
    private sharedService: SharedService;

    constructor(injector: Injector) {
        this.injector = injector;
        this.restService = this.injector.get(RestService);
        this.activeModal = this.injector.get(NgbActiveModal);
        this.formBuilder = this.injector.get(FormBuilder);
        this.notifierService = this.injector.get(NotifierService);
        this.translateService = this.injector.get(TranslateService);
        this.sharedService = this.injector.get(SharedService);
    }

    public ngOnInit(): void {
        /** On Initializing call the methods */
        this.profileFormAction();
        this.headers = new HttpHeaders({
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache, no-store, must-revalidate, max-age=0'
        });
        if (!isNullOrUndefined(this.profileID)) {
            this.isEdit = true;
            this.profileForm.patchValue({ name: this.profileName, description: this.profileDescription });
        } else {
            this.isAdd = true;
        }
    }

    /** On modal initializing forms  @public */
    public profileFormAction(): void {
        this.profileForm = this.formBuilder.group({
            name: ['', [Validators.required]],
            default: [null],
            description: ['', [Validators.required]],
            updatename: [''],
            updatedescription: ['']
        });
    }

    /** convenience getter for easy access to form fields */
    get f(): FormGroup['controls'] { return this.profileForm.controls; }

    /** On modal submit profileSubmit will called @public */
    public profileSubmit(): void {
        this.submitted = true;
        this.sharedService.cleanForm(this.profileForm);
        if (this.profileForm.invalid) {
            return;
        }
        if (!isNullOrUndefined(this.profileID)) {
            this.editProfile(this.profileType);
        } else {
            this.addProfile();
        }
    }
    /** Add profile @public */
    public addProfile(): void {
        const modalData: MODALCLOSERESPONSEDATA = {
            message: 'Done'
        };
        if (this.profileType === 'infra-config') {
            this.profileUrl = environment.K8SINFRACONFIGPROFILE_URL;
        } else if (this.profileType === 'infra-controller') {
            this.profileUrl = environment.K8SINFRACONTROLLERPROFILE_URL;
        } else if (this.profileType === 'app-profile') {
            this.profileUrl = environment.K8SAPPPROFILE_URL;
        } else if (this.profileType === 'resource-profile') {
            this.profileUrl = environment.K8SRESOURCEPROFILE_URL;
        }
        const apiURLHeader: APIURLHEADER = {
            url: this.profileUrl,
            httpOptions: { headers: this.headers }
        };

        this.isLoadingResults = true;
        delete this.profileForm.value.updatename;
        delete this.profileForm.value.updatedescription;
        const payload: INFRACONFIGPAYLOAD = {
            name: this.profileForm.value.name,
            description: this.profileForm.value.description
        };
        this.restService.postResource(apiURLHeader, payload).subscribe((result: {}) => {
            this.activeModal.close(modalData);
            this.isLoadingResults = false;
            this.notifierService.notify('success',
                this.translateService.instant('PAGE.K8S.PROFILECREATEDSUCCESSFULLY'));
        }, (error: ERRORDATA) => {
            this.restService.handleError(error, 'post');
            this.isLoadingResults = false;
        });
    }

    /** Edit profile @public */
    public editProfile(profileType: string): void {
        const modalData: MODALCLOSERESPONSEDATA = {
            message: 'Done'
        };
        this.isLoadingResults = true;
        if (profileType === 'infra-config') {
            this.profileUrl = environment.K8SINFRACONFIGPROFILE_URL + '/' + this.profileID;
        } else if (profileType === 'infra-controller') {
            this.profileUrl = environment.K8SINFRACONTROLLERPROFILE_URL + '/' + this.profileID;
        } else if (this.profileType === 'app-profile') {
            this.profileUrl = environment.K8SAPPPROFILE_URL + '/' + this.profileID;
        } else if (this.profileType === 'resource-profile') {
            this.profileUrl = environment.K8SRESOURCEPROFILE_URL + '/' + this.profileID;
        }
        const apiURLHeader: APIURLHEADER = {
            url: this.profileUrl,
            httpOptions: { headers: this.headers }
        };
        delete this.profileForm.value.name;
        delete this.profileForm.value.description;
        if (this.profileForm.value.updatename === '') {
            this.payload = {
                description: this.profileForm.value.updatedescription
            };
            delete this.profileForm.value.updatename;
        } else if (this.profileForm.value.updatedescription === '') {
            this.payload = {
                name: this.profileForm.value.updatename
            };
            delete this.profileForm.value.updatedescription;
        } else {
            this.payload = {
                name: this.profileForm.value.updatenamename,
                description: this.profileForm.value.updatedescription
            };
        }

        this.restService.patchResource(apiURLHeader, this.payload).subscribe((result: {}) => {
            this.activeModal.close(modalData);
            this.isLoadingResults = false;
            this.notifierService.notify('success',
                this.translateService.instant('PAGE.K8S.PROFILEEDITEDSUCCESSFULLY'));
        }, (error: ERRORDATA) => {
            this.restService.handleError(error, 'post');
            this.isLoadingResults = false;
        });
    }
}

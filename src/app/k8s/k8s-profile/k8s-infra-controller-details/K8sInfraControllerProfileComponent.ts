/*
 Copyright 2020 TATA ELXSI

 Licensed under the Apache License, Version 2.0 (the 'License');
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: SANDHYA JS (sandhya.j@tataelxsi.co.in)
*/
/**
 * @file K8sInfraConfigProfileComponent.ts.
 */
import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { CONFIGCONSTANT, ERRORDATA, MODALCLOSERESPONSEDATA } from 'CommonModel';
import { DataService } from 'DataService';
import { environment } from 'environment';
import { K8sActionComponent } from 'K8sActionComponent';
import { INFRACONFIGPAYLOAD, K8SCreateCLUSTERDATA, K8SCREATEDATADISPLAY } from 'K8sModel';
import { LocalDataSource } from 'ng2-smart-table';
import { RestService } from 'RestService';
import { Subscription } from 'rxjs';
import { SharedService } from 'SharedService';
import { K8sInfraConfigAddComponent } from '../k8s-infra-config-add/K8sInfraConfigAddComponent';
/**
 * Creating Component
 * @Component takes K8sInfraControllerProfileComponent.html as template url
 */
@Component({
    selector: 'app-infra-controller-profile',
    templateUrl: './K8sInfraControllerProfileComponent.html',
    styleUrls: ['./K8sInfraControllerProfileComponent.scss']
})
/** Exporting a class @exports K8sInfraControllerProfileComponent */
export class K8sInfraControllerProfileComponent implements OnInit, OnDestroy {
    /** To inject services @public */
    public injector: Injector;

    /** handle translate @public */
    public translateService: TranslateService;

    /** Data of smarttable populate through LocalDataSource @public */
    public dataSource: LocalDataSource = new LocalDataSource();

    /** Columns list of the smart table @public */
    public columnList: object = {};

    /** Settings for smarttable to populate the table with columns @public */
    public settings: object = {};

    /** operational State created data @public */
    public operationalStateFirstStep: string = CONFIGCONSTANT.k8OperationalStateFirstStep;

    /** operational State in creation data @public */
    public operationalStateSecondStep: string = CONFIGCONSTANT.k8OperationalStateStateSecondStep;

    /** operational State in deletion data @public */
    public operationalStateThirdStep: string = CONFIGCONSTANT.k8OperationalStateThirdStep;

    /** operational State failed deletion data @public */
    public operationalStateFourthStep: string = CONFIGCONSTANT.k8OperationalStateFourthStep;

    /** operational State failed creation data @public */
    public operationalStateFifthStep: string = CONFIGCONSTANT.k8OperationalStateFifthStep;

    /** Check the loading results @public */
    public isLoadingResults: boolean = true;

    /** Give the message for the loading @public */
    public message: string = 'PLEASEWAIT';

    /** Class for empty and present data @public */
    public checkDataClass: string;

    /** Instance of the rest service @private */
    private restService: RestService;

    /** dataService to pass the data from one component to another @private */
    private dataService: DataService;

    /** Formation of appropriate Data for LocalDatasource @private */
    private profileData: {}[] = [];

    /** Contains all methods related to shared @private */
    private sharedService: SharedService;

    /** Instance of the modal service @private */
    private modalService: NgbModal;

    /** Instance of subscriptions @private */
    private generateDataSub: Subscription;

    constructor(injector: Injector) {
        this.injector = injector;
        this.restService = this.injector.get(RestService);
        this.dataService = this.injector.get(DataService);
        this.sharedService = this.injector.get(SharedService);
        this.translateService = this.injector.get(TranslateService);
        this.modalService = this.injector.get(NgbModal);
    }
    /** Lifecyle Hooks the trigger before component is instantiate @public */
    public ngOnInit(): void {
        this.generateColumns();
        this.generateSettings();
        this.generateData();
        this.generateDataSub = this.sharedService.dataEvent.subscribe(() => { this.generateData(); });
    }

    /** smart table Header Colums @public */
    public generateColumns(): void {
        this.columnList = {
            name: { title: this.translateService.instant('NAME'), width: '20%', sortDirection: 'asc' },
            identifier: { title: this.translateService.instant('IDENTIFIER'), width: '15%' },
            state: {
                title: this.translateService.instant('GITSTATE'), width: '15%', type: 'html',
                filter: {
                    type: 'list',
                    config: {
                        selectText: 'Select',
                        list: [
                            { value: this.operationalStateFirstStep, title: this.operationalStateFirstStep },
                            { value: this.operationalStateSecondStep, title: this.operationalStateSecondStep },
                            { value: this.operationalStateThirdStep, title: this.operationalStateThirdStep },
                            { value: this.operationalStateThirdStep, title: this.operationalStateFourthStep },
                            { value: this.operationalStateThirdStep, title: this.operationalStateFifthStep }
                        ]
                    }
                },
                valuePrepareFunction: (cell: INFRACONFIGPAYLOAD, row: INFRACONFIGPAYLOAD): string => {
                    if (row.state === this.operationalStateFirstStep) {
                        return `<span class="icon-label" title="${row.state}">
                                 <i class="fas fa-clock text-success"></i>
                                 </span>`;
                    } else if (row.state === this.operationalStateSecondStep) {
                        return `<span class="icon-label" title="${row.state}">
                                 <i class="fas fa-spinner text-warning"></i>
                                 </span>`;
                    } else if (row.state === this.operationalStateThirdStep) {
                        return `<span class="icon-label" title="${row.state}">
                                 <i class="fas fa-spinner text-danger"></i>
                                 </span>`;
                    } else if (row.state === this.operationalStateFourthStep) {
                        return `<span class="icon-label" title="${row.state}">
                                 <i class="fas fa-times-circle text-danger"></i>
                                 </span>`;
                    } else if (row.state === this.operationalStateFifthStep) {
                        return `<span class="icon-label" title="${row.state}">
                                 <i class="fas fa-times-circle text-warning"></i>
                                 </span>`;
                    } else {
                        return `<span>${row.state}</span>`;
                    }
                }
            },
            created: { title: this.translateService.instant('CREATED'), width: '10%' },
            modified: { title: this.translateService.instant('MODIFIED'), width: '10%' },
            Actions: {
                name: 'Action', width: '5%', filter: false, sort: false, title: this.translateService.instant('ACTIONS'), type: 'custom',
                valuePrepareFunction: (cell: K8SCREATEDATADISPLAY, row: K8SCREATEDATADISPLAY): K8SCREATEDATADISPLAY => row,
                renderComponent: K8sActionComponent
            }
        };
    }

    /** smart table Data Settings @public */
    public generateSettings(): void {
        this.settings = {
            columns: this.columnList,
            actions: { add: false, edit: false, delete: false, position: 'right' },
            attr: this.sharedService.tableClassConfig(),
            pager: this.sharedService.paginationPagerConfig(),
            noDataMessage: this.translateService.instant('NODATAMSG')
        };
    }

    /** smart table listing manipulation @public */
    public onChange(perPageValue: number): void {
        this.dataSource.setPaging(1, perPageValue, true);
    }

    /** smart table listing manipulation @public */
    public onUserRowSelect(event: MessageEvent): void {
        Object.assign(event.data, { page: 'k8-infra-controller' });
        this.dataService.changeMessage(event.data);
    }

    /** Compose new Profile @public */
    public addProfile(): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        const modalRef: NgbModalRef = this.modalService.open(K8sInfraConfigAddComponent, { backdrop: 'static' });
        modalRef.componentInstance.profileType = 'infra-controller';
        modalRef.result.then((result: MODALCLOSERESPONSEDATA) => {
            if (result) {
                this.sharedService.callData();
            }
        }).catch((): void => {
            // Catch Navigation Error
        });
    }

    /**
     * Lifecyle hook which get trigger on component destruction
     */
    public ngOnDestroy(): void {
        this.generateDataSub.unsubscribe();
    }
    /** Generate profile object from loop and return for the datasource @public */
    public generateprofileData(profileData: INFRACONFIGPAYLOAD): INFRACONFIGPAYLOAD {
        return {
            name: profileData.name,
            identifier: profileData._id,
            created: this.sharedService.convertEpochTime(Number(profileData._admin.created)),
            modified: this.sharedService.convertEpochTime(Number(profileData._admin.modified)),
            description: profileData.description,
            pageType: 'infra-controller',
            state: profileData.state
        };
    }

    /** Fetching the data from server to Load in the smarttable @protected */
    protected generateData(): void {
        this.isLoadingResults = true;
        this.restService.getResource(environment.K8SINFRACONTROLLERPROFILE_URL).subscribe((profileDatas: K8SCreateCLUSTERDATA[]) => {
            this.profileData = [];
            profileDatas.forEach((profileData: INFRACONFIGPAYLOAD) => {
                const profileDataObj: INFRACONFIGPAYLOAD = this.generateprofileData(profileData);
                this.profileData.push(profileDataObj);
            });
            if (this.profileData.length > 0) {
                this.checkDataClass = 'dataTables_present';
            } else {
                this.checkDataClass = 'dataTables_empty';
            }
            this.dataSource.load(this.profileData).then((data: boolean) => {
                this.isLoadingResults = false;
            }).catch(() => {
                this.isLoadingResults = false;
            });
        }, (error: ERRORDATA) => {
            this.restService.handleError(error, 'get');
            this.isLoadingResults = false;
        });
    }
}

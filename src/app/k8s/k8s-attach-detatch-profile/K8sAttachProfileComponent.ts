/*
 Copyright 2020 TATA ELXSI

 Licensed under the Apache License, Version 2.0 (the 'License');
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: SANDHYA JS (sandhya.j@tataelxsi.co.in)
*/
/**
 * @file Attach Profile Component.
 */
import { HttpHeaders } from '@angular/common/http';
import { ChangeDetectorRef, Component, Injector, Input, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { NotifierService } from 'angular-notifier';
import { APIURLHEADER, ERRORDATA, MODALCLOSERESPONSEDATA } from 'CommonModel';
import { environment } from 'environment';
import { K8SCreateCLUSTERDATA, K8SPayload, ProfileMap, ProfileMappings } from 'K8sModel';
import { RestService } from 'RestService';
/**
 * Creating Component
 * @Component takes K8sAttachProfileComponent.html as template url
 */
@Component({
    selector: 'app-k8s-attach-profile',
    templateUrl: './K8sAttachProfileComponent.html',
    styleUrls: ['./K8sAttachProfileComponent.scss']
})
/** Exporting a class @exports K8sAttachProfileComponent */
export class K8sAttachProfileComponent implements OnInit {
    /** To inject services @public */
    public injector: Injector;

    /** Instance for active modal service @public */
    public activeModal: NgbActiveModal;

    /** FormGroup user Edit Account added to the form @ html @public */
    public attachForm: FormGroup;

    /** Form submission Add */
    public submitted: boolean = false;

    /** Form submission Add */
    public selectedData: string;

    /** Input contains Modal dialog component Instance @private */
    @Input() public userTitle: string;

    /** Input contains Modal dialog component Instance @private */
    @Input() public userID: string;

    /** Contains user details information @public */
    public userDetails: K8SCreateCLUSTERDATA[];

    /** Contains user details information @public */
    public filterDetails: K8SCreateCLUSTERDATA[];

    /** Contains user details information @public */
    public checkDetails: K8SCreateCLUSTERDATA[];

    /** Contains user details information @public */
    public profileDetails: K8SCreateCLUSTERDATA;

    /** Contains user details information @public */
    public count: number;

    /** Profile Mapping @public */
    public profileMap: ProfileMap = {};

    /** Check the loading results @public */
    public isLoadingResults: boolean = false;

    /** Give the message for the loading @public */
    public message: string = 'PLEASEWAIT';

    /** Input contains Modal dialog component Instance @public */
    @Input() public profileType: string;

    /** Input contains Modal dialog component Instance @public */
    @Input() public profileID: string;

    /** Give the message for the loading @public */
    public profileUrl: string;

    /** Give the message for the loading @public */
    public isAttach: boolean = false;

    /** Instance of the rest service @private */
    private restService: RestService;

    /** FormBuilder instance added to the formBuilder @private */
    private formBuilder: FormBuilder;

    /** Controls the header form @private */
    private headers: HttpHeaders;

    /** Notifier service to popup notification @private */
    private notifierService: NotifierService;

    /** Contains tranlsate instance @private */
    private translateService: TranslateService;

    /** Profile Form array @private */
    private attachFormArray: FormArray;

    /** Detect changes for the User Input */
    private cd: ChangeDetectorRef;

    constructor(injector: Injector) {
        this.injector = injector;
        this.formBuilder = this.injector.get(FormBuilder);
        this.restService = this.injector.get(RestService);
        this.activeModal = this.injector.get(NgbActiveModal);
        this.notifierService = this.injector.get(NotifierService);
        this.translateService = this.injector.get(TranslateService);
        this.cd = this.injector.get(ChangeDetectorRef);
    }

    /** Generate primitive params @public */
    get profileParamsBuilder(): FormGroup {
        return this.formBuilder.group({
            profile_name: [null, [Validators.required]]
        });
    }

    /** convenience getter for easy access to form fields */
    get f(): FormGroup['controls'] { return this.attachForm.controls; }

    /** Lifecyle Hooks the trigger before component is instantiate @public */
    public ngOnInit(): void {
        this.headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Accept: 'application/json',
            'Cache-Control': 'no-cache, no-store, must-revalidate, max-age=0'
        });
        this.initializeForm();
        this.generateData();
        this.generateProfile();
    }

    /** Initializing Form Action  @public */
    public initializeForm(): void {
        this.attachForm = this.formBuilder.group({
            infra_config_profiles: this.formBuilder.array([])
        });
    }

    /** Handle FormArray Controls @public */
    public getControls(): AbstractControl[] {
        return (this.attachForm.get('infra_config_profiles') as FormArray).controls;
    }

    /** Fetching the data from server to Load in the smarttable @public */
    public generateData(): void {
        if (this.profileID !== '') {
            this.isLoadingResults = true;
            if (this.profileType === 'infra-config') {
                this.profileUrl = environment.K8SCREATECLUSTER_URL + '/' + this.profileID + '/' + 'infra_config_profiles';
            } else if (this.profileType === 'infra-controller') {
                this.profileUrl = environment.K8SCREATECLUSTER_URL + '/' + this.profileID + '/' + 'infra_controller_profiles';
            } else if (this.profileType === 'app-profile') {
                this.profileUrl = environment.K8SCREATECLUSTER_URL + '/' + this.profileID + '/' + 'app_profiles';
            } else if (this.profileType === 'resource-profile') {
                this.profileUrl = environment.K8SCREATECLUSTER_URL + '/' + this.profileID + '/' + 'resource_profiles';
            }
            this.restService.getResource(this.profileUrl).subscribe((userDetail: K8SCreateCLUSTERDATA[]): void => {
                this.checkDetails = userDetail;
                this.filterDetails = this.checkDetails.filter((itemData: K8SCreateCLUSTERDATA): boolean => itemData.default === false);
                if (this.filterDetails.length !== 0) {
                    this.filterDetails.forEach((datas: K8SCreateCLUSTERDATA): void => {
                        let profileData: ProfileMappings[] = [];
                        profileData = this.filterDetails.map((item) => ({
                            name: item.name,
                            _id: item._id
                        }));
                        this.profileDetails = { infra_config_profiles: profileData };
                    });
                    this.count = this.profileDetails.infra_config_profiles.length;
                    this.loadMapping();
                } else {
                    this.profileDetails = { infra_config_profiles: [] };
                }
                this.isLoadingResults = false;
            }, (error: ERRORDATA): void => {
                this.isLoadingResults = false;
                this.restService.handleError(error, 'get');
            });
        }
    }

    /** Fetching the data from server to Load in the smarttable @public */
    public generateProfile(): void {
        this.isLoadingResults = true;
        if (this.profileType === 'infra-config') {
            this.profileUrl = environment.K8SINFRACONFIGPROFILE_URL;
        } else if (this.profileType === 'infra-controller') {
            this.profileUrl = environment.K8SINFRACONTROLLERPROFILE_URL;
        } else if (this.profileType === 'app-profile') {
            this.profileUrl = environment.K8SAPPPROFILE_URL;
        } else if (this.profileType === 'resource-profile') {
            this.profileUrl = environment.K8SRESOURCEPROFILE_URL;
        }
        this.restService.getResource(this.profileUrl).subscribe((userDetail: K8SCreateCLUSTERDATA[]): void => {
            this.userDetails = userDetail.filter((itemData: K8SCreateCLUSTERDATA): boolean => itemData.default === false);
            this.isLoadingResults = false;
        }, (error: ERRORDATA): void => {
            this.isLoadingResults = false;
            this.restService.handleError(error, 'get');
        });
    }

    /** Set all profile values to the form @public */
    public loadMapping(): void {
        this.profileDetails.infra_config_profiles.forEach((datas: ProfileMappings): void => {
            this.attachFormArray = this.attachForm.get('infra_config_profiles') as FormArray;
            const control = this.formBuilder.group({
                profile_name: [datas._id]
            });
            this.attachFormArray.push(control);
        });
    }

    /** Remove profile from the list @public */
    public removeMapping(index: number): void {
        this.attachFormArray.removeAt(index);
        this.isAttach = false;
    }

    /** Submit profile @public */
    public attachProfile(): void {
        this.submitted = true;
        const modalData: MODALCLOSERESPONSEDATA = {
            message: 'Done'
        };
        if (this.attachForm.invalid) { return; }
        if (this.profileType === 'infra-config') {
            this.profileUrl = environment.K8SCREATECLUSTER_URL + '/' + this.profileID + '/' + 'infra_config_profiles';
        } else if (this.profileType === 'infra-controller') {
            this.profileUrl = environment.K8SCREATECLUSTER_URL + '/' + this.profileID + '/' + 'infra_controller_profiles';
        } else if (this.profileType === 'app-profile') {
            this.profileUrl = environment.K8SCREATECLUSTER_URL + '/' + this.profileID + '/' + 'app_profiles';
        } else if (this.profileType === 'resource-profile') {
            this.profileUrl = environment.K8SCREATECLUSTER_URL + '/' + this.profileID + '/' + 'resource_profiles';
        }
        const apiURLHeader: APIURLHEADER = {
            url: this.profileUrl
        };
        this.profileMap.add_profile = [];
        this.attachForm.value.infra_config_profiles.forEach((res: ProfileMappings): void => {
            this.profileMap.add_profile.push({ id: res.profile_name });
        });
        this.profileMap.add_profile.splice(0, this.count);
        if (this.profileMap.add_profile.length !== 0) {
            if (!this.attachForm.dirty) {
                this.notifierService.notify('warning', this.translateService.instant('PAGE.TOPOLOGY.DATAEMPTY'));
                return;
            }
            this.isLoadingResults = true;
            this.restService.patchResource(apiURLHeader, this.profileMap).subscribe((result: {}): void => {
                this.isLoadingResults = false;
                this.activeModal.close(modalData);
                this.notifierService.notify('success', this.translateService.instant('PAGE.K8S.ATTACHEDSUCCESSFULLY'));
            }, (error: ERRORDATA): void => {
                this.isLoadingResults = false;
                this.restService.handleError(error, 'patch');
            });
        } else {
            this.notifierService.notify('error', this.translateService.instant('PAGE.K8S.WARNING'));
        }
    }

    /** Add extra mapping and set empty profile @public */
    public addMapping(): void {
        this.attachFormArray = this.attachForm.get('infra_config_profiles') as FormArray;
        this.attachFormArray.push(this.profileParamsBuilder);
        if ((this.attachFormArray.value.length === this.count + 1) || (this.attachFormArray.value.length === 1)) {
            this.isAttach = true;
        }
    }

    /** Remove profile @public */
    public deleteProfile(getProfile: ProfileMappings): void {
        const modalData: MODALCLOSERESPONSEDATA = {
            message: 'Done'
        };
        const removeProfile: ProfileMap = { remove_profile: [] };
        removeProfile.remove_profile = [{ id: getProfile._id }];
        if (this.profileType === 'infra-config') {
            this.profileUrl = environment.K8SCREATECLUSTER_URL + '/' + this.profileID + '/' + 'infra_config_profiles';
        } else if (this.profileType === 'infra-controller') {
            this.profileUrl = environment.K8SCREATECLUSTER_URL + '/' + this.profileID + '/' + 'infra_controller_profiles';
        } else if (this.profileType === 'app-profile') {
            this.profileUrl = environment.K8SCREATECLUSTER_URL + '/' + this.profileID + '/' + 'app_profiles';
        } else if (this.profileType === 'resource-profile') {
            this.profileUrl = environment.K8SCREATECLUSTER_URL + '/' + this.profileID + '/' + 'resource_profiles';
        }
        const apiURLHeader: APIURLHEADER = {
            url: this.profileUrl
        };
        this.isLoadingResults = true;
        this.restService.patchResource(apiURLHeader, removeProfile).subscribe((result: {}): void => {
            this.isLoadingResults = false;
            this.activeModal.close(modalData);
            this.notifierService.notify('success', this.translateService.instant('PAGE.K8S.DETATCHEDSUCCESSFULLY'));
        }, (error: ERRORDATA): void => {
            this.isLoadingResults = false;
            this.restService.handleError(error, 'patch');
        });
    }
}

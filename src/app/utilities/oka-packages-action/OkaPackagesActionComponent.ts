/*
 Copyright 2020 TATA ELXSI

 Licensed under the Apache License, Version 2.0 (the 'License');
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: SANDHYA JS (sandhya.j@tataelxsi.co.in)
*/
/**
 * @file okapackageAction Component
 */
import { Component, Injector } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { MODALCLOSERESPONSEDATA } from 'CommonModel';
import { ComposePackages } from 'ComposePackages';
import { DeleteComponent } from 'DeleteComponent';
import { SharedService } from 'SharedService';
import { VNFData } from 'VNFDModel';

/**
 * Creating component
 * @Component takes OkaPackagesActionComponent.html as template url
 */
@Component({
    templateUrl: './OkaPackagesActionComponent.html',
    styleUrls: ['./OkaPackagesActionComponent.scss']
})
/** Exporting a class @exports OkaPackagesActionComponent */
export class OkaPackagesActionComponent {
    /** To get the value from the vnfpackage via valuePrepareFunction default Property of ng-smarttable @public */
    public value: VNFData;

    /** To inject services @public */
    public injector: Injector;

    /** Check the loading results for loader status @public */
    public isLoadingDownloadResult: boolean = false;

    /** Give the message for the loading @public */
    public message: string = 'PLEASEWAIT';

    /** Contains state @public */
    public state: string;

    /** Instance of the modal service @private */
    private modalService: NgbModal;

    /** Variables holds oka ID @private */
    private okaID: string;

    /** Contains all methods related to shared @private */
    private sharedService: SharedService;

    constructor(injector: Injector) {
        this.injector = injector;
        this.sharedService = this.injector.get(SharedService);
        this.modalService = this.injector.get(NgbModal);
    }

    /**
     * Lifecyle Hooks the trigger before component is instantiate
     */
    public ngOnInit(): void {
        this.okaID = this.value.identifier;
        this.state = this.value.state;
    }

    /** Delete NS Config oka @public */
    public deleteoka(): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        const modalRef: NgbModalRef = this.modalService.open(DeleteComponent, { backdrop: 'static' });
        modalRef.result.then((result: MODALCLOSERESPONSEDATA) => {
            if (result) {
                this.sharedService.callData();
            }
        }).catch((): void => {
            // Catch Navigation Error
        });
    }

    /** Set instance for oka Edit @public */
    public okaEdit(): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        const modalRef: NgbModalRef = this.modalService.open(ComposePackages, { backdrop: 'static' });
        modalRef.componentInstance.params = { id: this.okaID, page: 'oka-packages-edit', operationType: 'edit' };
        modalRef.result.then((result: MODALCLOSERESPONSEDATA) => {
            if (result) {
                this.sharedService.callData();
            }
        }).catch((): void => {
            // Catch Navigation Error
        });
    }
}

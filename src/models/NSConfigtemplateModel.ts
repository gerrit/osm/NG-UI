/*
 Copyright 2020 TATA ELXSI

 Licensed under the Apache License, Version 2.0 (the 'License');
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: SANDHYA JS (sandhya.j@tataelxsi.co.in)
 */
/**
 * @file  Model for NS Config template related information.
 */
export interface NSCONFIG {
    _admin: ADMINDETAILS;
    _id: string;
    vnf: ADDITIONAL[];
    nsdId: string;
    name: string;
}

export interface ADMINDETAILS {
    created?: string;
    modified?: string;
    onboardingState?: string;
    operationalState?: string;
    projects_read?: string[];
    projects_write?: string[];
}

export interface ADDITIONAL {
    vdu: PARAMS[];
    'member-vnf-index'?: string;
}

export interface PARAMS {
    id?: string;
    'vim-flavor-id'?: string;
}

/** Interface for VNFData */
export interface NSConfigData {
    nsdId?: string;
    identifier?: string;
    created?: string;
    modified?: string;
    onboardingState?: string;
    operationalState?: string;
    name?: string;
    config?: {};
    vnf?: {};
    additionalParamsForVnf?: {};
    additionalParamsForNs?: {};
    vld?: {};
}
